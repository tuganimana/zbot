# Zbot

Bot for the Flare network.

If you have received tips it is easy to start tipping, just type
```
Ztip @recipient 100000000000000000
```
to tip 0.1 FXRP. Note that you have to hit enter in the suggestions list of recipients rather than typing out the whole name, because of reasons.

To withdraw you first have to register an address:
```
Zregister 0x123ADDRESS
```
Then simply
```
Zwithdraw
```

To deposit either send money to the contract address with your registered address or run the deposit script in the deposit folder, first change the ID to your discord ID: https://support.discord.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-

Then run it like so:
```
SECRET=123YOURPRIVATEKEY go run deposit.go
```

After updating Zbot.sol in contracts folder run:
```
rm -r build
solc --abi --bin Zbot.sol -o build
abigen --bin=./build/Zbot.bin --abi=./build/Zbot.abi --pkg=zbot --out=Zbot.go
mv Zbot.go build
SECRET=PRIVATEKEY go run deploy.go
```
Then copy the new contract address to flare.go and deposit.go

To run the bot first do go build then
```
SECRET=123PRIVATEKEYOFCONTRACTOWNER ./zbot -t Abc.bot.token
```