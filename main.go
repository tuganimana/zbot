package main

import (
	"flag"
	"fmt"
	"math/big"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	zbot "./contracts/build"
	"github.com/bwmarrin/discordgo"
)

// Variables used for command line parameters
var (
	Token    string
	instance *zbot.Zbot
)

func init() {
	flag.StringVar(&Token, "t", "", "Bot Token")
	flag.Parse()
}

func main() {
	instance = loadContract()

	// Create a new Discord session using the provided bot token.
	dg, err := discordgo.New("Bot " + Token)
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}
	// Register the messageCreate func as a callback for MessageCreate events.
	dg.AddHandler(messageCreate)

	dg.Identify.Intents = discordgo.MakeIntent(discordgo.IntentsGuildMessages | discordgo.IntentsDirectMessages)

	// Open a websocket connection to Discord and begin listening.
	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}
	//channels, _ := dg.GuildChannels(dg.State.Guilds[0].ID)
	//fmt.Println(len(channels))
	//fmt.Println(len(channels[2].Messages))

	// Wait here until CTRL-C or other term signal is received.
	fmt.Println("Bot is now running.  Press CTRL-C to exit.")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	// Cleanly close down the Discord session.
	dg.Close()
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	fmt.Println("Read message", m.Content, " from ", m.Author.Username, "with ID", m.Author.ID)
	// If the message is "ping" reply with "Pong!"
	if m.Content == "ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// If the message is "pong" reply with "Ping!"
	if m.Content == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}

	spl := strings.Split(m.Content, " ")
	switch spl[0] {
	case "Zblock":
		s.ChannelMessageSend(m.ChannelID, "The current Coston block is: "+currentBlock())
	case "Zregister":
		id := "discord " + m.Author.ID
		//fmt.Println(id)
		err := Zregister(instance, spl[1], id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Registered address "+spl[1]+" to user with ID "+m.Author.ID)
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	case "Zbalance":
		id := "discord " + m.Author.ID
		result, err := instance.Balance(nil, id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Balance of user with id "+id+" is "+result.String())
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	case "Zaddress":
		id := "discord " + m.Author.ID
		result, err := instance.GetAddress(nil, id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Address of user with id "+id+" is "+result.Hex())
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	case "Ztip":
		if len(spl) < 3 {
			s.ChannelMessageSend(m.ChannelID, "Not enough arguments: Ztip @recipient 5")
			return
		}
		if spl[1][0:3] != "<@!" { //sometimes the tag looks like <@& and sometimes <@
			s.ChannelMessageSend(m.ChannelID, "Malformed tag.")
			return
		}
		idrec := "discord " + spl[1][3:len(spl[1])-1]
		idsender := "discord " + m.Author.ID
		amount, err := strconv.Atoi(spl[2])
		if err != nil {
			s.ChannelMessageSend(m.ChannelID, "Could not parse value.")
			return
		}
		amountbig := big.NewInt(int64(amount))
		bal, _ := instance.Balance(nil, idsender)
		if amountbig.Cmp(bal) > 0 || bal.Cmp(big.NewInt(0)) == 0 {
			s.ChannelMessageSend(m.ChannelID, "Not enough balance.")
			return
		}
		err = Ztip(instance, idsender, idrec, amountbig)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Tip request sent to blockchain!")
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	case "Zwithdraw":
		id := "discord " + m.Author.ID
		addr, _ := instance.GetAddress(nil, id)
		if addr.Hex() == "0x0000000000000000000000000000000000000000" {
			s.ChannelMessageSend(m.ChannelID, "No account registered with user ID, type Zregister 0x123")
			return
		}
		err := Zwithdraw(instance, id)
		if err == nil {
			s.ChannelMessageSend(m.ChannelID, "Withdrew all funds!")
		} else {
			s.ChannelMessageSend(m.ChannelID, "Error: "+err.Error())
		}
	default:
		dm, _ := ComesFromDM(s, m)
		if dm {
			s.ChannelMessageSend(m.ChannelID, "Try Zblock or read the README at: https://gitlab.com/marcus.appelros/zbot")
		}
	}
}

// ComesFromDM returns true if a message comes from a DM channel
func ComesFromDM(s *discordgo.Session, m *discordgo.MessageCreate) (bool, error) {
	channel, err := s.State.Channel(m.ChannelID)
	if err != nil {
		if channel, err = s.Channel(m.ChannelID); err != nil {
			return false, err
		}
	}

	return channel.Type == discordgo.ChannelTypeDM, nil
}
